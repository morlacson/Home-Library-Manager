#include "library.h"
#include "ui_library.h"
#include <QDebug>
#include <QPixmap>
#include <QSettings>
#include <QSqlError>
#include "json.h"
Library::Library(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Library)
{
    this->setupUi();
    this->show();
}

void Library::setupUi()
{
    ui->setupUi(this);
    settings = new QSettings("HomeLibraryManager", "settings");
    settingsWindow = new Settings();
    newBook = new NewEdit();
    editBook = new NewEdit();
    about = new About();
    source = QSqlDatabase::addDatabase("QSQLITE");
    toSource = new QSqlQuery(source);
    table = new QSqlTableModel(this, source);
    this->settingsSetup();
    db = settings->value("DatabasePath").toString() + settings->value("DatabaseName").toString();
    this->setupDatabase();
    ui->MainTable->setModel(table);
    ui->MainTable->hideColumn(6);
    ui->MainTable->hideColumn(7);
    ui->MainTable->resizeColumnsToContents();
    ui->toolButton->setDefaultAction(ui->Newbook);
    ui->toolButton_2->setDefaultAction(ui->actionDelete);
    ui->toolButton_3->setDefaultAction(ui->actionEdit);
    ui->toolButton_4->setDefaultAction(ui->Import);
    ui->toolButton_5->setDefaultAction(ui->Export);
    ui->toolButton_6->setDefaultAction(ui->Settings);
    ui->toolButton_7->setDefaultAction(ui->ShowHide);
    ui->label_4->setScaledContents(true);
    this->setupConnections();
}

void Library::settingsSetup()
{
    if(!settings->contains("DatabasePath"))
        settings->setValue("DatabasePath", qgetenv("HOME") + "/.HLM/");
    if(!settings->contains("DatabaseName"))
        settings->setValue("DatabaseName", "HLM.db");
    if(!settings->contains("autoresize"))
        settings->setValue("autoresize", true);
}
void Library::setupDatabase()
{
    source.setDatabaseName(db);
    if (!source.open())
    {
        qDebug() << "Error";
        qDebug() << source.lastError();
        ui->MainTable->hide();
        ui->groupBox->hide();
        ui->Newbook->setEnabled(false);
        ui->actionDelete->setEnabled(false);
        ui->Export->setEnabled(false);
        ui->Import->setEnabled(false);
        ui->actionDelete->setEnabled(false);
        ui->actionEdit->setEnabled(false);
        ui->ShowHide->setEnabled(false);
        if(source.lastError().nativeErrorCode() == "14")
        {
            ui->errorWidget->show();
        }
    } else
    {
        ui->MainTable->show();
        ui->errorWidget->hide();
        ui->groupBox->show();
        toSource->exec("CREATE DATABASE lib;");
        toSource->exec("USE lib;");
        toSource->exec("CREATE TABLE Library(Name TEXT, Author TEXT, Category TEXT, Publisher TEXT, Series TEXT, ISBN TEXT, Description TEXT, imageurl TEXT);");
        table->setTable("Library");
        table->setEditStrategy(QSqlTableModel::OnFieldChange);
        table->select();
    }
}

void Library::setupConnections()
{
     connect(ui->MainTable->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(changed()));
}


Library::~Library()
{
    table->submitAll();
    delete ui;
}

void Library::on_actionDelete_triggered()
{
    if(ui->MainTable->currentIndex().isValid())
    {
        QMessageBox::StandardButton deleteIt = QMessageBox::warning(this, "Delete Boook", "Are you sure wont to delete this book from database?", QMessageBox::Yes | QMessageBox::No);
        if (deleteIt == QMessageBox::Yes)
        {
            table->removeRow(ui->MainTable->currentIndex().row());
            table->submitAll();
            table->select();
            if(settings->value("autoresize").toBool())
                ui->MainTable->resizeColumnsToContents();
        }
    }
}

void Library::on_lineEdit_textChanged(const QString &arg)
{
    QString arg1 = arg.simplified();
    if(arg1 != "")
    {
        if(ui->comboBox->currentIndex() == 0)
        {
            for(int i = 0; i < table->rowCount(); ++i)
            {

                if(table->data(ui->MainTable->model()->index(i, 0)).toString().contains(arg1, Qt::CaseInsensitive))
                {
                    ui->MainTable->showRow(i);
                } else
                {
                    ui->MainTable->hideRow(i);
                }

            }
        } else if (ui->comboBox->currentIndex() == 1)
        {
            for(int i = 0; i < table->rowCount(); ++i)
            {

                if(table->data(ui->MainTable->model()->index(i, 1)).toString().contains(arg1, Qt::CaseInsensitive))
                {
                    ui->MainTable->showRow(i);
                } else
                {
                    ui->MainTable->hideRow(i);
                }

            }
        } else if (ui->comboBox->currentIndex() == 2)
        {
            for(int i = 0; i < table->rowCount(); ++i)
            {

                if(table->data(ui->MainTable->model()->index(i, 2)).toString().contains(arg1, Qt::CaseInsensitive))
                {
                    ui->MainTable->showRow(i);
                } else
                {
                    ui->MainTable->hideRow(i);
                }
            }
        } else if (ui->comboBox->currentIndex() == 3)
        {
            for(int i = 0; i < table->rowCount(); ++i)
            {

                if(table->data(ui->MainTable->model()->index(i, 3)).toString().contains(arg1, Qt::CaseInsensitive))
                {
                    ui->MainTable->showRow(i);
                } else
                {
                    ui->MainTable->hideRow(i);
                }
            }
        } else if (ui->comboBox->currentIndex() == 4)
        {
            for(int i = 0; i < table->rowCount(); ++i)
            {

                if(table->data(ui->MainTable->model()->index(i, 4)).toString().contains(arg1, Qt::CaseInsensitive))
                {
                    ui->MainTable->showRow(i);
                } else
                {
                    ui->MainTable->hideRow(i);
                }
            }
        } else if (ui->comboBox->currentIndex() == 5)
        {
            for(int i = 0; i < table->rowCount(); ++i)
            {

                if(table->data(ui->MainTable->model()->index(i, 5)).toString().contains(arg1, Qt::CaseInsensitive))
                {
                    ui->MainTable->showRow(i);
                } else
                {
                    ui->MainTable->hideRow(i);
                }
            }
        }
    } else
    {
        for(int i = 0; i < table->rowCount(); ++i)
        {
            if(ui->MainTable->isRowHidden(i))
            {
                ui->MainTable->showRow(i);
            }
        }
    }
}

void Library::on_actionAbout_triggered()
{
    About a;
    a.setModal(true);
    a.exec();
}



void Library::changed()
{
    qDebug() << "Signal emitted" << ui->MainTable->selectionModel()->selectedRows().size();
    if(ui->MainTable->selectionModel()->selectedRows().size() == 1)
    {
        if(!ui->MainTable->selectionModel()->selectedRows().isEmpty())
        {
            ui->label->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(0)).toString());
            ui->label_2->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(1)).toString());
            ui->label_3->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(2)).toString());
            ui->label_5->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(3)).toString());
            ui->label_8->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(4)).toString());
            ui->label_7->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(5)).toString());
            ui->label_6->setText(table->data(ui->MainTable->currentIndex().siblingAtColumn(6)).toString());
            QNetworkAccessManager *manager = new QNetworkAccessManager(this);
            reply = manager->get(QNetworkRequest(table->data(ui->MainTable->currentIndex().siblingAtColumn(7)).toString()));
            connect(reply, SIGNAL(readyRead()), this, SLOT(read()));
            if(settings->value("autoresize").toBool())
                ui->MainTable->resizeColumnsToContents();
            editBook->name = table->data(ui->MainTable->currentIndex().siblingAtColumn(0)).toString();
            editBook->author = table->data(ui->MainTable->currentIndex().siblingAtColumn(1)).toString();
            editBook->cat = table->data(ui->MainTable->currentIndex().siblingAtColumn(2)).toString();
            editBook->pub = table->data(ui->MainTable->currentIndex().siblingAtColumn(3)).toString();
            editBook->Series = table->data(ui->MainTable->currentIndex().siblingAtColumn(4)).toString();
            editBook->ISBN = table->data(ui->MainTable->currentIndex().siblingAtColumn(5)).toString();
            editBook->desc = table->data(ui->MainTable->currentIndex().siblingAtColumn(6)).toString();
            editBook->image = table->data(ui->MainTable->currentIndex().siblingAtColumn(7)).toString();
            ui->actionEdit->setEnabled(true);
        }
    } else
    {
        ui->label->setText("");
        ui->label_2->setText("");
        ui->label_3->setText("");
        ui->label_5->setText("");
        ui->label_8->setText("");
        ui->label_7->setText("");
        ui->label_6->setText("");
        ui->label_4->setPixmap(QPixmap(0, 0));
        ui->actionEdit->setEnabled(false);
    }
}


void Library::on_actionEdit_triggered()
{
    if(ui->MainTable->selectionModel()->selectedRows().size() == 1)
    {
        editBook->setModal(true);
        editBook->setWindowTitle("Edit Book");

        editBook->reset();
        if (editBook->exec())
        {
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(0), editBook->name);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(1), editBook->author);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(2), editBook->cat);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(3), editBook->pub);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(4), editBook->Series);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(5), editBook->ISBN);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(6), editBook->desc);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(7), editBook->image);
            table->submitAll();
            if(settings->value("autoresize").toBool())
                ui->MainTable->resizeColumnsToContents();
        }
    }
}

void Library::on_Newbook_triggered()
{
    int newbook = table->rowCount();
    table->insertRow(newbook);
    ui->MainTable->selectRow(newbook);
    NewEdit a;
    a.setModal(true);
    a.setWindowTitle("New Book");   
    if(a.exec())
    {
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(0), a.name);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(1), a.author);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(2), a.cat);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(3), a.pub);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(4), a.Series);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(5), a.ISBN);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(6), a.desc);
        table->setData(ui->MainTable->currentIndex().siblingAtColumn(7), a.image);
        if(settings->value("autoresize").toBool())
            ui->MainTable->resizeColumnsToContents();
    }
    table->submitAll();
    table->select();
}

void Library::on_Save_Changes_triggered()
{
    table->submitAll();
    table->select();
}


void Library::on_Import_triggered()
{
    QFileDialog a(this);
    a.setFileMode(QFileDialog::ExistingFile);
    a.setNameFilter(tr("JSON (*.json)"));
    a.setViewMode(QFileDialog::Detail);
    a.setDirectory("~/");
    if (a.exec())
    {
            QJsonDocument import = loadJson(a.selectedFiles().first());

            for (int i = 0; i < import.array().size(); ++i)
            {
                int temp = table->rowCount();
                table->insertRow(temp);
                table->setData(table->index(temp, 0) ,import.array().at(i).toObject().value("Name").toString());
                table->setData(table->index(temp, 1) ,import.array().at(i).toObject().value("Author").toString());
                table->setData(table->index(temp, 2) ,import.array().at(i).toObject().value("Category").toString());
                table->setData(table->index(temp, 3) ,import.array().at(i).toObject().value("Publisher").toString());
                table->setData(table->index(temp, 4) ,import.array().at(i).toObject().value("Series").toString());
                table->setData(table->index(temp, 5) ,import.array().at(i).toObject().value("ISBN").toString());
                table->setData(table->index(temp, 6) ,import.array().at(i).toObject().value("Description").toString());
                table->setData(table->index(temp, 7) ,import.array().at(i).toObject().value("Image").toString());
                table->submitAll();
            }
            table->submitAll();
            if(settings->value("autoresize").toBool())
                ui->MainTable->resizeColumnsToContents();

    }
}

void Library::on_Export_triggered()
{
    QFileDialog a(this);
    a.setFileMode(QFileDialog::AnyFile);
    a.setNameFilter(tr("JSON (*.json)"));
    a.setViewMode(QFileDialog::Detail);
    a.setDirectory("~/");
    a.setAcceptMode(QFileDialog::AcceptSave);
    a.setDefaultSuffix(".json");
    if (a.exec())
    {
        QJsonDocument b;
        QJsonArray d;
        for (int i = 0; i < table->rowCount(); ++i)
        {
            QJsonObject c;
            c["Name"] = table->data(table->index(i, 0)).toString();
            c["Author"] = table->data(table->index(i, 1)).toString();
            c["Category"] = table->data(table->index(i, 2)).toString();
            c["Publisher"] = table->data(table->index(i, 3)).toString();
            c["Series"] = table->data(table->index(i, 4)).toString();
            c["ISBN"] = table->data(table->index(i, 5)).toString();
            c["Description"] = table->data(table->index(i, 6)).toString();
            c["Image"] = table->data(table->index(i, 7)).toString();
            d.append(c);
        }
        b.setArray(d);
        saveJson(b, a.selectedFiles().first());
    }
}



void Library::read()
{
    QPixmap a;
    a.loadFromData(reply->readAll());
    ui->label_4->setPixmap(a);
}

void Library::on_Settings_triggered()
{
    Settings a;
    a.setSpaces(this->settings);
    if(a.exec() == QDialog::Accepted)
    {
        settings->setValue("DatabasePath", a.path);
        settings->setValue("DatabaseName", a.name);
        settings->setValue("autoresize", a.autoresize);
    }
}
void Library::on_ShowHide_triggered()
{
    if(ui->groupBox->isHidden())
    {
        ui->groupBox->show();
    } else
    {
        ui->groupBox->hide();
    }
}

void Library::on_MainTable_doubleClicked()
{
    if(ui->MainTable->selectionModel()->selectedRows().size() == 1)
    {
        editBook->setModal(true);
        editBook->setWindowTitle("Edit Book");

        editBook->reset();
        if (editBook->exec())
        {
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(0), editBook->name);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(1), editBook->author);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(2), editBook->cat);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(3), editBook->pub);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(4), editBook->Series);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(5), editBook->ISBN);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(6), editBook->desc);
            table->setData(ui->MainTable->currentIndex().siblingAtColumn(7), editBook->image);
            table->submitAll();
            if(settings->value("autoresize").toBool())
                ui->MainTable->resizeColumnsToContents();
        }
    }
}
