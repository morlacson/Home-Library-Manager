#include "settings.h"
#include "ui_settings.h"
#include <QFileDialog>

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_toolButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    if (dialog.exec())
    {
        ui->lineEdit->setText(dialog.selectedFiles().first());
        if(ui->lineEdit->text()[ui->lineEdit->text().count()] != QChar('/'))
        {
            QString tmp = ui->lineEdit->text();
            tmp.append('/');
            ui->lineEdit->setText(tmp);
        }
    }
}

void Settings::setSpaces(QSettings *setting)
{
    ui->lineEdit->setText(setting->value("DatabasePath").toString());
    ui->lineEdit_2->setText(setting->value("DatabaseName").toString());
    ui->checkBox->setChecked(setting->value("autoresize").toBool());
}

void Settings::on_Settings_accepted()
{
    if(ui->lineEdit->text()[ui->lineEdit->text().count()] != QChar('/'))
    {
        QString tmp = ui->lineEdit->text();
        tmp.append('/');
        ui->lineEdit->setText(tmp);
    }
    this->path = ui->lineEdit->text();
    this->name = ui->lineEdit_2->text();
    this->autoresize = ui->checkBox->isChecked();
}
