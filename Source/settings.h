#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr);
    ~Settings();
    void setSpaces(QSettings *setting);
    QString path;
    QString name;
    bool autoresize;
private slots:
    void on_toolButton_clicked();

    void on_Settings_accepted();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
