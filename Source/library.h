#ifndef LIBRARY_H
#define LIBRARY_H
#define DEBUG
#define CONFIGFILE "~/.HLM/config.json"

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QMessageBox>
#include <QFileDialog>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSettings>
#include "settings.h"
#include "newedit.h"
#include "about.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Library; }
QT_END_NAMESPACE

class Library : public QMainWindow
{
    Q_OBJECT

public:
    Library(QWidget *parent = nullptr);
    ~Library();

private slots:


    void on_actionDelete_triggered();

    void on_lineEdit_textChanged(const QString &arg);

    void on_actionAbout_triggered();

    void changed();

    void on_actionEdit_triggered();

    void on_Newbook_triggered();

    void on_Save_Changes_triggered();


    void on_Import_triggered();

    void on_Export_triggered();
    void read();

    void on_Settings_triggered();

    void on_ShowHide_triggered();

    void on_MainTable_doubleClicked();

private:
    Ui::Library *ui;
    QSqlDatabase source;
    QSqlQuery *toSource;
    QSqlTableModel *table;
    QNetworkReply *reply;
    QString db;
    QSettings *settings;
    Settings *settingsWindow;
    NewEdit *newBook;
    NewEdit *editBook;
    About *about;
    void setupUi();
    void settingsSetup();
    void setupDatabase();
    void setupConnections();
};
#endif // LIBRARY_H
