#ifndef NEWEDIT_H
#define NEWEDIT_H

#include <QDialog>

namespace Ui {
class NewEdit;
}

class NewEdit : public QDialog
{
    Q_OBJECT

public:
    explicit NewEdit(QWidget *parent = nullptr);
    ~NewEdit();
    QString name;
    QString author;
    QString cat;
    QString pub;
    QString Series;
    QString ISBN;
    QString desc;
    QString image;
    void reset();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::NewEdit *ui;
};

#endif // NEWEDIT_H
