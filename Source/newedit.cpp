#include "newedit.h"
#include "ui_newedit.h"

NewEdit::NewEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewEdit)
{
    ui->setupUi(this);
}

NewEdit::~NewEdit()
{
    delete ui;
}

void NewEdit::on_buttonBox_accepted()
{
    name = ui->lineEdit->text();
    author = ui->lineEdit_2->text();
    cat = ui->lineEdit_3->text();
    pub = ui->lineEdit_4->text();
    Series = ui->lineEdit_5->text();
    ISBN = ui->lineEdit_6->text();
    desc = ui->textEdit->toPlainText();
    image = ui->lineEdit_7->text();
}

void NewEdit::reset()
{
     ui->lineEdit->setText(name);
     ui->lineEdit_2->setText(author);
     ui->lineEdit_3->setText(cat);
     ui->lineEdit_4->setText(pub);
     ui->lineEdit_5->setText(Series);
     ui->lineEdit_6->setText(ISBN);
     ui->textEdit->setPlainText(desc);
     ui->lineEdit_7->setText(image);
}

